import React, { createContext, useContext, useState } from 'react';
import './App.css';
import {
  Card, makeStyles, CardHeader, Button, Grid, TextField, Typography, List,
  Container, useTheme, AppBar, Toolbar, Divider, IconButton, Drawer, CssBaseline,
  Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper,
  InputLabel, FormControl, MenuItem, Select
} from '@material-ui/core';
import { Route, Switch, Link } from 'react-router-dom';
import './materialize.min.css';
import logo from './logo.png';

function json(response) {
  return response.json();
}
function status(response) {
  if (response.status !== 200) {
    console.log("Error : " + response.status);
    return Promise.reject(new Error(response.statusText));
  } else {
    return Promise.resolve(response);
  }
}
function error(error) {

  console.log("Error : " + error);
}

class Teams extends React.Component {
  constructor() {
    super()
    this.state = {
      data: []
    }
  }
  componentDidMount() {
    var base_url = "https://api.football-data.org/v2/";
    let api_token = '9aaf7b8ec81e4ecf8d3292f6594b58b3';
    let datas = []
    let that = this
    fetch(base_url + "competitions/2021/teams", {
      method: 'GET',
      headers: {
        'X-Auth-Token': api_token,
      },
    })
      .then(status)
      .then(json)
      .then(function (data) {
        data.teams.forEach(function (team) {
          datas.push({
            crest: team.crestUrl,
            name: team.shortName,
            venue: team.venue
          })
        })
        that.setState({ data: datas })
      })
      .catch(error);
  }
  renderTeam() {
    let data = this.state.data
    return (
      data.map((team, index) => {
        return (
          <div key={index} className="col s12 m2">
            <div className="card team-card">
              <div className="card-image" style={{ maxHeight: 180, maxWidth: 180 }} >
                <img className="responsive-img" src={team.crest} />
              </div>
              <div className="card-content" style={{ padding: 0 }}>
                <span className="card-title truncate" style={{ fontSize: 16 }}>{team.name}</span>
                <p className="flow-text truncate" style={{ fontSize: 10 }}>{team.venue}</p>
              </div>
            </div>
          </div>

        )
      })
    )
  }
  render() {
    return (<div className="row">
      <div id="card-team" className="col s12">
        <div className="container" >
          {this.renderTeam()}
        </div>
      </div>
    </div>)
  }
}

class Home extends React.Component {
  render() {
    return (<div className="Container text-center">
      <img src={logo} alt="Logo" className="centerimg" />
      <h2 className="centertxt">Welcome to Premier League</h2>
    </div>)
  }
}

class Standings extends React.Component {
  constructor() {
    super()
    this.state = {
      data: [],
      isLoading: true
    }
  }
  componentDidMount() {
    var base_url = "https://api.football-data.org/v2/";
    let api_token = '9aaf7b8ec81e4ecf8d3292f6594b58b3';
    let datas = []
    let that = this
    fetch(base_url + "competitions/2021/standings", {
      method: 'GET',
      headers: {
        'X-Auth-Token': api_token,
      },
    })
      .then(status)
      .then(json)
      .then(function (data) {
        let stands = data.standings.filter(function (elem) { return elem.type === "TOTAL"; })
        stands.forEach(function (team) {
          team.table.forEach(function (table) {

            datas.push({
              pos: table.position,
              crest: table.team.crestUrl,
              name: table.team.name,
              pg: table.playedGames,
              won: table.won,
              draw: table.draw,
              lost: table.lost,
              gf: table.goalsFor,
              ga: table.goalsAgainst,
              gd: table.goalDifference,
              points: table.points
            })
            that.setState({
              isLoading: false,
              data: datas
            })
          })
        })
      })
      .catch(error);
    datas.forEach(function (team) {
    })

  }
  renderTable() {
    let data = this.state.data

    return (
      data.map((team, index) => {
        return (<tr key={index}>
          <td>{team.pos}</td>
          <td className="crest-team"><img width="18px" height="18px" className="responsive-img" src={team.crest} /></td>
          <td>{team.name}</td>
          <td>{team.pg}</td>
          <td>{team.won}</td>
          <td>{team.draw}</td>
          <td>{team.lost}</td>
          <td>{team.gf}</td>
          <td>{team.ga}</td>
          <td>{team.gd}</td>
          <td>{team.points}</td>
        </tr>)
      })
    )

  }
  render() {
    const { data, isLoading } = this.state


    return (
      <div className="container">
        {/* {data !== null ? this.renderTable(data) : <div></div>} */}
        <table className="responsive-table highlight striped centered">
          <thead id="head-league">
            <tr>
              <th>Pos</th>
              <th className="crest-team"></th>
              <th>Club</th>
              <th>Played</th>
              <th>Won</th>
              <th>Draw</th>
              <th>Lost</th>
              <th>GF</th>
              <th>GA</th>
              <th>GD</th>
              <th>Points</th>
            </tr>
          </thead>
          <tbody id="standing-league">
            {this.renderTable()}

          </tbody>
        </table>
      </div>
    )
  }
}
const usedStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  appColor: {
    backgroundColor: '#686d76'
  }
}));
const Routing = () => {
  const classes = usedStyles();
  return (
    <AppBar position="static" className={classes.appColor}>
      <Toolbar>

        <Typography variant="h5" className={classes.title}>
          Premier League
                   </Typography>
        <Link to="/">
          <Button color="inherit">Home</Button>
        </Link>
        <Link to="/teams">
          <Button color="inherit">Teams</Button>
        </Link>
        <Link to="/standings">
          <Button color="inherit">Standings</Button>
        </Link>
      </Toolbar>
    </AppBar>
  )
}
const App = (props) => {

  return (
    <main>
      <Routing />
      <Switch >
        <Route path="/" exact component={Home} />
        <Route path="/standings" exact component={Standings} />
        <Route path="/teams" exact component={Teams} />
      </Switch>

    </main>
  )
}
export default App;